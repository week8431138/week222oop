function calculateDaysInMonth() {
    var month = parseInt(document.getElementById("month").value);
    var leapYear = parseInt(document.getElementById("leapYear").value);
  
  var daysInMonth;
  
  if (month === 2) {
  daysInMonth = leapYear ? 29 : 28;
  } else if (month === 4 || month === 6 || month === 9 || month === 11) {
  daysInMonth = 30;
  } else {
  daysInMonth = 31;
  }
  
  document.getElementById("result").innerText = daysInMonth;
  }
