function printAge(age) {
  var ageInput = document.getElementById("age");
  var age = ageInput.value;
  var result = document.getElementById("result");
  
  if (age == 1) {
    result.innerHTML = age + " год";
   } else if (age % 10 == 1 && age % 100 != 11) {
    result.innerHTML = age + " год";
   } else if ((age % 10 == 2 || age % 10 == 3 || age % 10 == 4) && (age % 100 != 12 && age % 100 != 13 && age % 100 != 14)) {
    result.innerHTML = age + " года";
   } else {
    result.innerHTML = age + " лет";
  }
}
