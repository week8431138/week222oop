function drawPyramid() {
    // Получаем введенные данные из input
    const height = parseInt(document.getElementById("heightInput").value);
    
    // Получаем элемент canvas
    const canvas = document.getElementById("canvas");
    const ctx = canvas.getContext("2d");
    
    // Очищаем канву перед каждым рисованием
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    
    // Задаем настройки для рисования пирамиды
    const brickWidth = 50;
    const brickHeight = 15;
    const pyramidLeft = canvas.width / 2 - (brickWidth * height) / 2;
    const pyramidTop = canvas.height - (brickHeight * height);
    
    // Рисуем пирамиду
    for (let row = 0; row < height; row++) {
    for (let brick = 0; brick < height - row; brick++) {
    const brickLeft = pyramidLeft + brick * brickWidth + row * (brickWidth / 2);
    const brickTop = pyramidTop + row * brickHeight;
    
    ctx.fillRect(brickLeft, brickTop, brickWidth, brickHeight);
    }
    }
    }
    
