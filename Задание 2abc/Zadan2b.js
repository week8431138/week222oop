function sieveOfEratosthenes(n) {
    // Создаем массив для всех чисел от 2 до n
    let primes = [];
    for (let i = 0; i <= n; i++) {
    primes[i] = true;
    }
    
    // Исключаем числа, которые являются составными
    for (let p = 2; p * p <= n; p++) {
    if (primes[p] === true) {
    for (let i = p * p; i <= n; i += p) {
    primes[i] = false;
    }
    }
    }
    
    // Выводим простые числа
    let result = [];
    for (let p = 2; p <= n; p++) {
    if (primes[p] === true) {
    result.push(p);
    }
    }
    
    return result;
    }
    
    function findPrimes() {
    // Получаем значение n от пользователя
    let n = parseInt(document.getElementById("numberInput").value);
    
    // Проверяем корректность введенного значения
    if (isNaN(n) || n < 2) {
    document.getElementById("result").innerText = "Введите целое число больше 1.";
    return;
    }
    
    // Вызываем алгоритм решета Эратосфена и выводим результат
    let primes = sieveOfEratosthenes(n);
    document.getElementById("result").innerText = primes.join(", ");
}
