function findMedian() {
    // Получаем введенные данные из input
    const numbersString = document.getElementById("numbersInput").value;
    
    // Разбиваем строку с числами на массив
    const numbers = numbersString.split(",").map(Number);
    
    // Сортируем массив чисел
    numbers.sort((a, b) => a - b);
    
    // Находим медиану
    let median;
    const middleIndex = Math.floor(numbers.length / 2);
    if (numbers.length % 2 === 0) {
    // Если количество чисел четное
    median = (numbers[middleIndex - 1] + numbers[middleIndex]) / 2;
    } else {
    // Если количество чисел нечетное
    median = numbers[middleIndex];
    }
    
    // Выводим результат
    const result = document.getElementById("result");
    result.textContent = `Медиана: ${median}`;
}
