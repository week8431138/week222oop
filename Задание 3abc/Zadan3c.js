function mergeSort(arr) {
    if (arr.length <= 1) {
    return arr;
    }
    
    const middleIndex = Math.floor(arr.length / 2);
    const leftArr = arr.slice(0, middleIndex);
    const rightArr = arr.slice(middleIndex);
    
    const sortedLeftArr = mergeSort(leftArr);
    const sortedRightArr = mergeSort(rightArr);
    
    return merge(sortedLeftArr, sortedRightArr);
    }
    
    function merge(leftArr, rightArr) {
    let mergedArr = [];
    
    let leftIndex = 0;
    let rightIndex = 0;
    
    while (leftIndex < leftArr.length && rightIndex < rightArr.length) {
    if (leftArr[leftIndex] <= rightArr[rightIndex]) {
    mergedArr.push(leftArr[leftIndex]);
    leftIndex++;
    } else {
    mergedArr.push(rightArr[rightIndex]);
    rightIndex++;
    }
    }
    
    mergedArr = mergedArr.concat(leftArr.slice(leftIndex)).concat(rightArr.slice(rightIndex));
    
    return mergedArr;
    }
    
    function sortNumbers() {
    const input = document.getElementById("inputNumbers").value;
    const numbers = input.split(",").map(Number);
    
    const sortedNumbers = mergeSort(numbers);
    
    document.getElementById("result").textContent = sortedNumbers.join(", ");
    }
    
