function countMaxNumbers() {
    // Получаем введенные данные из input
    const numbersString = document.getElementById("numbersInput").value;
    
    // Разбиваем строку с числами на массив
    const numbers = numbersString.split(",").map(Number);
    
    // Находим максимальное число в массиве
    const maxNumber = Math.max(...numbers);
    
    // Считаем количество чисел, равных максимальному
    let count = 0;
    for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] === maxNumber) {
    count++;
    }
    }
    
    // Выводим результат
    const result = document.getElementById("result");
    result.textContent = `Количество чисел, равных максимальному: ${count}`;
}
